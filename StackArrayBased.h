/*
 * StackArrayBased.h
 *
 *  Created on: Oct 29, 2016
 *      Author: mohamed
 */

#ifndef STACKARRAYBASED_H_
#define STACKARRAYBASED_H_
//==============================================================//

#define MAXSIZE 3

//=============================================================//

typedef struct stackentery{
int x[MAXSIZE];
int y;
}StackEntery;

//=========================================================//

typedef struct stack{
	StackEntery Element;
    int top;
}Stack;

//=====================================================//
typedef enum returnvalue
{
StackFuLL=0,
StackBushSUC ,
StackBoobSUC,
StackEmpty,
StackCreatSUC,
StackCleardSUC,
SUC,
FAIL
}ReturnValue;
//==================================================//

ReturnValue Status;

//=================================================//
ReturnValue CreateStack(Stack *sp);
ReturnValue Push(StackEntery E ,Stack *sp);
ReturnValue pop(Stack *sp);
ReturnValue StackSize(int *Size,Stack *sp);
ReturnValue ClearStack(Stack *sp);


#endif /* STACKARRAYBASED_H_ */
